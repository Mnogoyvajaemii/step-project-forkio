"use strict"

const burgerIcon = document.querySelector(".burger-icon");
const bugerIconLines= document.querySelectorAll(".burger-icon__line");
const burgerMenuLinks = document.querySelectorAll(".header-page__menu--item");
const menu = document.querySelector(".header-page__menu");
const fiolRect = document.querySelector(".fiol-rectangle");

function changeBurgerIcon () {
    bugerIconLines.forEach((element) => {
        element.classList.toggle("burger-icon__line--active");
    })
}

burgerIcon.addEventListener("click", () =>{
        changeBurgerIcon ();
        menu.classList.toggle("header-page__menu--active");
    
  })

burgerMenuLinks.forEach( (element) => 
    element.addEventListener("mouseenter", (event) => {
       event.target.append(fiolRect);
       fiolRect.classList.add("fiol-rectangle--active");
    }) );

    menu.addEventListener("mouseleave", () => {
        fiolRect.remove();
})

document.addEventListener("click", (event) => {
    let target = event.target;
    let headerMenu = target == menu || menu.contains(target);
    let burger = target == burgerIcon || target.closest(".burger-icon");
    let headerMenuActive = menu.classList.contains('header-page__menu--active');

    if (!headerMenu && !burger && headerMenuActive) {
        menu.classList.toggle("header-page__menu--active");
        changeBurgerIcon ();
    }
})
      

window.addEventListener("resize", (event) => {
    if (event.target.screen.availWidth > 480) {
        menu.classList.remove('header-page__menu--active');
        bugerIconLines.forEach((element) => {
            element.classList.remove("burger-icon__line--active");
        })
    }
})
